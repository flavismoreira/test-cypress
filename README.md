# test-cypress

## INSTALAR

### 1. Clonando o repositório:
```
git clone git@github.com:flaviafreitas/test-cypress.git
cd test-cypress
```

### 2. Dependencias
```
- npm v.7.0.14
- node v.15.3.0
- cypress v.6.0.1
```

## TESTES
```
Para rodar o teste:
- acessar a pasta test-cypress no terminal
- executar o comando: npx cypress open
- o cypress exibirá os testes implementados
- clicar em Running integration tests
```

## RESULTADOS
```
- total de testes: 7
- tempo de execucao: 42,89s
- evidencia de teste: test_passed.png
```
